# -*- coding: utf-8 -*-
"""
Created on Thu Jan  5 14:49:39 2023

@author: Michael Nauge (Université de Poitiers)

"""

import re
import os
import glob
import pandas as pd

markupStart = "[["
markupStop = "]]"

def searchOcc(pattern, listTextFilename):
    #print("pattern",pattern)
    dfOcc = pd.DataFrame(columns=['filename','pattern','idline','line'])

    for textFilename in listTextFilename:
        
        #print(f'search in {textFilename}')
        with open(textFilename, 'r', encoding="utf-8-sig",newline="\r\n") as filecur:
            #Pour chaque ligne du fichier
            for line in filecur:
                # chercher toutes les occurrences du pattern
                result = re.finditer(pattern, line)

                # pour chaque occurrence
                for match_obj in result:
                    # print each re.Match object
                    #print(match_obj)

                    # extract each matching number
                    # print(f'[[{match_obj.group()}]]')
                    
                    strAvantMatch = line[0:match_obj.span(0)[0]]
                    occ = line[match_obj.span(0)[0]:match_obj.span(0)[1]]
                    strApresMatch = line[match_obj.span(0)[1]:]
                    
                    idline = getAlignedTxtIdline(line)
                    #strOcc = f'{strAvantMatch}[[{occ}]]{strApresMatch}'
                    strOcc = f'{strAvantMatch}{markupStart}{occ}{markupStop}{strApresMatch}'

                
                    #print(f'{idline} : {strOcc}')
                    
                    folder, filename = os.path.split(textFilename)

                    dicRow =  {'filename':[filename]}
                    dicRow['pattern']=[pattern]
                    dicRow['idline'] =[idline]
                    dicRow['line'] = [strOcc]
                    dfRow = pd.DataFrame(data=dicRow)
                    
                    dfOcc = pd.concat([dfOcc, dfRow], ignore_index=True)
                    
    
    #nbOcc = len(dfOcc)
    #print(f'finded occ.: {nbOcc}')

    return dfOcc
    
    
    

def searchLinkedOcc(dataFolder, dfOcc):
    # a partir d'un dataframe d'occurrence 
    # (généré par searchOcc)
    # aller chercher la ligne correspondante dans les fichiers alignés
    #vpour sortir un dataframe resultat dfOccLinked
    
    
    # sortir la liste des idsources distinct
    # pour chaque idsource chercher ses fichiers liés
    dicLinkFile = {}
    distinctIdSource = dfOcc['filename'].unique()
    #print(distinctIdSource)
    for distFilename in distinctIdSource:
        dicLinkFile[distFilename] = getListLinkedFilename(dataFolder+distFilename)
        
    #print(dicLinkFile)
    #dfOccLinked = pd.DataFrame(columns=['lineSource','lineLinked','annotation','annotation2','notes','idSource','idLinked','idline','pattern'])
    
    dfOccLinked = dfOcc.copy()
    
    dfOccLinked.rename(columns = {'filename':'idSource','line':'lineSource'},inplace=True)
    
    dfOccLinked['annotation'] = ""
    dfOccLinked['annotation2'] = ""
    dfOccLinked['notes'] = ""
    dfOccLinked['idLinked'] = dfOccLinked.apply(lambda x: dicLinkFile[x['idSource']][0],1)
    
    #dfOccLinked['lineLinked'] = dfOccLinked.apply(lambda x: getLineFromIdline(dataFolder, dicLinkFile[x['idSource']][0], x['idline']),1)
    dfOccLinked['lineLinked'] = dfOccLinked.apply(lambda x: getLineFromIdline(dataFolder, x['idLinked'], x['idline']),1)
 


    dfOccLinked = dfOccLinked[['lineSource','lineLinked','annotation','annotation2','notes','idSource','idLinked','idline','pattern']]
    return dfOccLinked


def convertAlignedTxt2ParcolabTei(pathFileIn, pathFileOut):
    """
    transformer les fichiers aligné (produit par alinea et reséparés)
    en fichier xml tei compatible parcolab
        
    Parameters
    pathFileIn : STR
    	le chemin vers le ficher à transformer
        
    pathFileOut : STR
        le chemin vers le fichier transformé
        

        
    Returns
    -------
    success : BOOL
    """
    
    with open(pathFileIn, encoding="utf-8-sig", mode = "r") as ftxt, open(pathFileOut,  encoding="utf-8", mode = "w") as thTei:
        
        # ecrire le header
        thTei.write(getTeiHeader())
        

        thTei.write('<text>\r')
        thTei.write('  <body>\r')
        thTei.write('  <div>\r')
        thTei.write('  <p>\r')
        
        for row in ftxt:
            rs = row.strip()
            # enlever la numérotation
            content = re.sub(r'\[\d+\]\t *', '',rs)
            #thTei.write(f'  <p><s>{content}</s></p>\r')            
            thTei.write(f'  <s>{content}</s>\r')

        thTei.write('  </p>\r')
        thTei.write('  </div>\r')
        thTei.write('  </body>\r')
        thTei.write('</text>\r')
    

def getTeiHeader():
    strT = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\r'
    strT += '<TEI id="">\r'
    strT += '  <teiHeader>\r'
    strT += '    <fileDesc>\r'
    strT += '      <titleStmt></titleStmt>\r'
    strT += '      <publicationStmt></publicationStmt>\r'
    strT += '	 <respStmt></respStmt>\r'
    strT += '      <sourceDesc></sourceDesc>\r'
    strT += '    </fileDesc>\r'
    strT += '    <encodingDesc></encodingDesc>\r'
    strT += '   <profileDesc></profileDesc>\r'
    strT += '    <revisionDesc></revisionDesc>\r'
    strT += '  </teiHeader>\r'
    return strT    
    


    
    
def convertAlignedTxt2Html(pathFileIn, pathFileOut):
    """
    transformer les fichiers aligné (produit par alinea et reséparés)
    en fichier html 
        
    Parameters
    pathFileIn : STR
    	le chemin vers le ficher à transformer
        
    pathFileOut : STR
        le chemin vers le fichier transformé
        

        
    Returns
    -------
    success : BOOL
    """
    
    # --- BONUS ---
    # a partir du pathFileIn
    # trouver dans le dossier s'il existe d'autre fichiers
    # avec une autre extension de langue
    # -------------
    folder, filename = os.path.split(pathFileIn)

    
    listLinkedFilename = getListLinkedFilename(pathFileIn)
    
    #print(filename, "is linked to", listLinkedFilename)
    
    with open(pathFileIn, encoding="utf-8-sig", mode = "r") as ftxt, open(pathFileOut,  encoding="utf8", mode = "w") as thtml:
        
        # ecrire le header
        thtml.write(getHtmlHeader(filename+ "html view"))
        
        #ecrire le titre dans le body
        h1 = f'<h1>{filename} : html view</h1>\r'
        thtml.write(h1)

        
        for row in ftxt:
            rs = row.strip()
            
            i = getAlignedTxtIdline(rs)
            
            #thtml.write("{}".format(row).encode('utf8'))
            strRow = f'<p id={i}>'
            
            # bonus ajout des lien vers la meme ligne dans l'autre langue
          
            
            for linkedFiles in listLinkedFilename:
                #auth,tit,lang = getAuthTitleLangFromFilename(linkedFiles)
                tit,lang = getTitLangFromFilename(linkedFiles)
                lang = lang.lower()

                linkedFilesHtml = linkedFiles.replace(".txt",".html")
                #print("linkedFilesHtml",linkedFilesHtml)
                strRow += f'<a href="{linkedFilesHtml}#{i}">{lang}<</a>'
                
                
            strRow += f' {rs}</p>\r'
            #'<a href="">
            #thtml.write(row)
            thtml.write(strRow)
        
        thtml.write('</body>')

            
def getHtmlHeader(title):
    strH = "<!DOCTYPE html>\r"
    strH += "<html>\r"
    strH += "<head>\r"

    strH += '<meta charset="utf-8">\r'
    strH += f'<title>{title}</title>\r'
    strH += '<link rel="stylesheet" href="../r.css" />\r'
    strH += '</head>\r'

    strH += '<body>\r'
    return strH


def convertDfOccLinked2HtmlView(dfOccLinked, pathFileOut):
    
    
    # 'lineSource','lineLinked','annotation','annotation2','notes','idSource','idLinked','idline','pattern'
    
    with open(pathFileOut,  encoding="utf8", mode = "w") as occLinkedHtml:
        # ecrire le header
        occLinkedHtml.write(getHtmlHeader("html Linked Occ. View"))
        
        for index, row in dfOccLinked.iterrows():
            #print(row['langSrc'], row['langDst'])
            occLinkedHtml.write('<div>')
            occLinkedHtml.write("<h2 id="+str(index)+"> Occ. num "+str(index)+"</h2>\r\n")
            occLinkedHtml.write("<h3> "+row["idSource"]+" "+row["idLinked"]+"</h3>\r\n")

            
            occLinkedHtml.write("<p>"+'<a href="../corpusHtml/'+row["idSource"].replace(".txt",".html")+'#'+str(row['idline'])+'"> ȹ </a>'+row['lineSource'].replace('[[','<b>').replace(']]','</b>')+"</p>"+"\r\n")
            #occLinkedHtml.write("<p>"+row['lineLinked'].replace('[[','<b>').replace(']]','</b>')+"</p>"+"\r\n")
            occLinkedHtml.write("<p>"+'<a href="../corpusHtml/'+row["idLinked"].replace(".txt",".html")+'#'+str(row['idline'])+'"> ȹ </a>'+row['lineLinked'].replace('[[','<b>').replace(']]','</b>')+"</p>"+"\r\n")

            occLinkedHtml.write('</div>')
            
        occLinkedHtml.write('</body>')
    
   
def getListLinkedFilename(pathFileIn):
    # a partir du pathFileIn
    # sortir la partie dossier et la partie filename
    # du filename sortir l'extension de lang
    # trouver dans le dossier s'il existe d'autre fichiers
    # avec une autre extension de langue
    # -------------
    
    folder, filename = os.path.split(pathFileIn)
    #print(folder)
    #print(filename)
    #extraire auteur,titre, langue du filename
    #auth, tit, lang = getAuthTitleLangFromFilename(filename)
    tit, lang = getTitLangFromFilename(filename)
    print("tit",tit)
    print("lang",lang)
    
    listDesFichiersAuto = glob.glob(folder+"/"+tit+'*txt')
    
    listLinkedFiles = []
    
    for findedFilename in listDesFichiersAuto:
        folderL, filenameL = os.path.split(findedFilename)
        #print(filenameL)
        filenameLLower = filenameL.lower()
        #if not(filename==filenameL):
        filenameLower = filename.lower()
        if not(filenameLower==filenameLLower):

            listLinkedFiles.append(filenameL)
        
    #print(listLinkedFiles)
    return listLinkedFiles
    
    
def getAuthTitleLangFromFilename(filename):
    # on part du principe
    
    #chercher les lettres entre le dernier _ et l'extension .txt
    lettresFinales = "(.*?)\-(.+)_(\w+)\.txt"
    match = re.search(lettresFinales, filename, re.IGNORECASE)
    
    #l'extraire
    if match:
        return match.group(1), match.group(2),match.group(3)
    #le retourner
    else:
        print("probleme pas de nom de fichier de la forme Auteur-Titre_Lang.txt")
        return -1
        
def getTitLangFromFilename(filename):
    
    #chercher les lettres entre le dernier _ et l'extension .txt
    lettresFinales = "(.*)_(\w+)\.txt$"
    match = re.search(lettresFinales, filename, re.IGNORECASE)
    
    #l'extraire
    if match:
        return match.group(1),match.group(2)
    #le retourner
    else:
        print("probleme pas de nom de fichier de la forme Titre_Lang.txt")
        return -1
        
    
def getAlignedTxtIdline(textLine):
    
    #chercher le nombre entre crochet en début de ligne
    nombreEntreCrochet = "^\[(\d+)\]"
    match = re.search(nombreEntreCrochet, textLine, re.IGNORECASE)
    
    #l'extraire
    if match:
        return match.group(1)
    #le retourner
    else:
        print("probleme pas de idline")
        return -1
   
    
def getDicListFilenameByLang(folder):
    
    dicListFilenameByLang = {}
    
    listFichiers = glob.glob(f'{folder}/*txt')
    
    for pathFile in listFichiers:
        folder, filename = os.path.split(pathFile)
        #auth,tit,lang = getAuthTitleLangFromFilename(filename)
        tit, lang = getTitLangFromFilename(filename)
        lang = lang.lower()
        if lang in dicListFilenameByLang:
            dicListFilenameByLang[lang].append(filename)
        else:
            dicListFilenameByLang[lang] = [filename]
            
    return dicListFilenameByLang
    
    
    
def getLineFromIdline(dataFolder, pathFileIn, idline):
    
    with open(dataFolder+pathFileIn, 'r',encoding="utf-8-sig",newline="\r\n") as fileCur:
        for line in fileCur:
            #print(line)
            interestingLine = re.search( "^\["+str(idline)+"\]", line)
            
            if interestingLine:
                return line
            else:
                pass
    return ""

#convertAlignedTxt2Html('./datas/corpusInput/GRAFE-Lit-FrEn-sample-TXT-utf8/Beigbeder-Francs_FR.txt','./datas/corpusOutput/corpusHtml/Beigbeder-Francs_FR.html')
#convertAlignedTxt2Html('./datas/corpusInput/GRAFE-Lit-FrEn-sample-TXT-utf8/Beigbeder-Francs_EN.txt','./datas/corpusOutput/corpusHtml/Beigbeder-Francs_EN.html')
#convertAlignedTxt2ParcolabTei('./datas/corpusInput/Beigbeder-Francs_FR.txt','./datas/corpusOutput/corpusTei/Beigbeder-Francs_FR.xml')


#getAuthTitleLangFromFilename("Swift-Light-AL_EN.txt")

#print(getListLinkedFilename('./datas/corpusInput/Beigbeder-Francs_fr.txt'))
#print(getListLinkedFilename('./datas/corpusInput/1-MD2000_fr.txt'))

#print(getDicListFilenameByLang('./datas/corpusInput/'))

#searchOcc("\w*'*amour\w*",['./datas/corpusInput/GRAFE-Lit-FrEn-sample-TXT-utf8/Beigbeder-Francs_FR.txt'])

#print(getTitLangFromFilename("1-MD2000_tr_en.txt"))

