import pandas as pd
import pypandoc

import glob
from pathlib import Path


def convertParaconcAnsi2Utf8(fileAnsi,fileUtf8):
    with open(fileAnsi, encoding="cp1252", mode = "r") as fAnsi, open(fileUtf8, mode = "wb") as fUtf8:
        for row in fAnsi:
            fUtf8.write("{}".format(row).encode('utf8'))

def getConcordances(filePath):
    listTuple = []
    with open(filePath, encoding="utf8", mode = "r") as fp:
        concordLeft = None
        concordRight = None

        for cnt, line in enumerate(fp):
            if len(line)<= 1:               
                listTuple.append((concordLeft,concordRight))
                concordLeft = None
                concordRight = None
            else:
                #print("Line {}: {}".format(cnt, line))
                if concordLeft == None:
                    concordLeft = line
                else:
                    concordRight = line
    return listTuple

def concordancesToDf(listTup):
    dfOut = pd.DataFrame(columns = ['langSrc', 'langDst'])
    
    for lineSrc,lineDest in listTup:
        dicTmp = {'langSrc':lineSrc,'langDst':lineDest}
        dfOut = dfOut.append(dicTmp, ignore_index=True)
    
    return dfOut

def dfConcordToMd(dfConcord, fileMdOut):
    with open(fileMdOut, "w", encoding="utf8") as fout:
    
        for index,row in dfConcord.iterrows():
            #print(row['langSrc'], row['langDst'])
            fout.write("# Occ. num "+str(index+1)+"\r\n")
            
            fout.write("* "+row['langSrc'].replace('[[','**').replace(']]','**')+"\r\n")
            fout.write("* "+row['langDst'].replace('[[','**').replace(']]','**')+"\r\n")

def massiveConvert(pathIn, pathOut):
    listFiles = glob.glob(pathIn+"*.txt")
    listFileOut = []
    for fileCur in listFiles:
        filenameCur = Path(fileCur).stem
        if not(filenameCur=="readme"):
            listFileOut.append(filenameCur)

            # pour chaque fichier d'entree
            # convertir le txt ansi en utf8
            # convertir le txt utf8 en tableur
            # sauvegarder le tableur
            # convertir le tableur en markdown
            # convertir le markdown en docx
            # convertir le markdown en pdf (finalement non)

            filenameCurUtf8 = pathOut+filenameCur+"_utf8.txt"
            convertParaconcAnsi2Utf8(fileCur, filenameCurUtf8)

            listTup = getConcordances(filenameCurUtf8)
            df = concordancesToDf(listTup)

            filenameXlsOut = pathOut+filenameCur+"_utf8.xls"
            df.to_excel(filenameXlsOut, index=True)

            filenameMarkdownOut = pathOut+filenameCur+"_utf8.md"
            dfConcordToMd(df,filenameMarkdownOut)

            filenameDocxOut = pathOut+filenameCur+"_utf8.docx"
            output = pypandoc.convert_file(filenameMarkdownOut, 'docx', outputfile=filenameDocxOut)
            assert output == ""
            
            
            filenameHtmlOut = pathOut+filenameCur+"_utf8.html"
            output = pypandoc.convert_file(filenameMarkdownOut, 'html', outputfile=filenameHtmlOut)
            assert output == ""  

            #filenamePdfOut = pathOut+filenameCur+"_utf8.pdf"
            #output = pypandoc.convert_file(filenameMarkdownOut, 'pdf', outputfile=filenamePdfOut)
            #assert output == ""        
        
    return listFileOut
        